# Estrutura das pastas

### app 
Arquivos da aplicação referente a interface/back-end angularjs
### css  
Folhas de estilo para a interface
### fonts 
Fonts
### js  
Arquivos javascripts
### pages  
Contem os arquivos do front-end
### services 
Contem os arquivos do back-end - api
### sql  
Contem o arquivo banco_phprova.sql, necessario para criar a tabela tarefas

## Configuração

Executar o script banco_phprova.sql para criar o banco de dados da aplicação.

## A aplicação foi construida na url

http://provaphp.ifloripa.com.br/questao04/

Autor : José Sidney A. Ribeiro

https://bitbucket.org/jsribeiro123/provaphp




