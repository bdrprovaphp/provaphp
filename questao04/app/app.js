var app = angular.module('myApp', ['ngRoute']);

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        title: 'Tarefas',
        templateUrl: 'pages/tarefas.html',
        controller: 'listaTarefasCtrl'
      })
      .when('/edita-tarefa/:tarefaID', {
        title: 'Editar Tarefa',
        templateUrl: 'pages/edita-tarefa.html',
        controller: 'edicaoTarefaCtrl',
        resolve: {
          tarefa: function(services, $route){
            var tarefaID = $route.current.params.tarefaID;
            return services.getTarefa(tarefaID);
          }
        }
      })
      .otherwise({
        redirectTo: '/'
      });
}]);

app.controller('listaTarefasCtrl', function ($scope, services) {
    console.log('controller lista tarefas ');
    services.getTarefas().then(function(data){
        console.log(data);
        $scope.tarefas = data.data;
    });
});

app.controller('edicaoTarefaCtrl', function ($scope, $rootScope, $location, $routeParams, services, tarefa) {
    
    var tarefaID = ($routeParams.tarefaID) ? parseInt($routeParams.tarefaID) : 0;
    
    $rootScope.title = (tarefaID > 0) ? 'Editar' : 'Adicionar';
    $scope.buttonText = (tarefaID > 0) ? 'Alterar' : 'Adicionar';
    
      var original = tarefa.data;
      original._id = tarefaID;
      
      $scope.tarefa = angular.copy(original);
      $scope.tarefa._id = tarefaID;

      $scope.isClean = function() {
        return angular.equals(original, $scope.tarefa);
      }

      $scope.deleteTarefa = function(tarefa) {
          
        $location.path('/');
        
          if(confirm("Deletar tarefa , confirma !?")==true)
            services.deleteTarefa(tarefa.tarefaNumber);
          };

      $scope.saveTarefa = function(tarefa) {
        $location.path('/');
        if (tarefaID <= 0) {
            services.insertTarefa(tarefa);
        }
        else {
            services.updateTarefa(tarefaID, tarefa);
        }
    };
});

app.factory("services", ['$http', function($http) {
        
  var serviceBase = 'services/'
  
    var obj = {};
    
    obj.getTarefas = function(){
        return $http.get(serviceBase + 'tarefas').then(function (results) {
            console.log(results);
                return results;
        });
    }

    obj.getTarefa = function(tarefaID){
        return $http.get(serviceBase + 'tarefa?id=' + tarefaID);
    }

    obj.insertTarefa = function (tarefa) {
            return $http.post(serviceBase + 'insertTarefa', tarefa).then(function (results) {
                return results;
        });
    };

    obj.updateTarefa = function (id,tarefa) {
        return $http.post(serviceBase + 'updateTarefa', {id:id, tarefa:tarefa}).then(function (status) {
            return status.data;
        });
    };

    obj.deleteTarefa = function (id) {
        return $http.delete(serviceBase + 'deleteTarefa?id=' + id).then(function (status) {
            return status.data;
        });
    };

    return obj;   
}]);

