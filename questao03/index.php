<?php

class DbConnect
{
        const DB_SERVER = "127.0.0.1";
        const DB_USER = "root";
        const DB_PASSWORD = "";
        const DB = "php";

        private $mysqli = NULL;
        
        public function __construct(){
                parent::__construct();				
                $this->dbConnect();				
        }

        private function dbConnect(){
                $this->mysqli = new mysqli(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD, self::DB);
        }  
}

 class MyUserClass extends DbConnect
 { 
    public function getUserList(){	

        $query="select name from user";

        $results = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);

            if($results->num_rows > 0){
                sort($results);
            }
            else {
                $results = 0;   
            }

            return $results;
    }
 }

 
